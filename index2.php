<?php 
	include 'action.php';
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<script src="https://kit.fontawesome.com/de83bf60e8.js" crossorigin="anonymous"></script>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Everglow</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="stylesheet.css">
	</head>
	<body>
		<div class="container-fluid" style="padding-left: 0;padding-right: 0">
	<!-- NavBar -->
<nav id="nav-bar" class="navbar fixed-top navbar-expand-md shadow-sm p-3 mb-5 rounded" style="background:#C4E2BF;">
    <a href="" class="navbar-brand mr-0 text-primary"><img id="logo"src="img/everglow.png" style="width:50%;"></a> 
    <button class="navbar-toggler navbar-light ml-1" type="button" data-toggle="collapse" data-target="#collapsingNavbar2" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"> </span>
    </button>
    <div class="navbar-collapse collapse justify-content-center align-items-center w-100" id="collapsingNavbar2">
        <ul class="navbar-nav mx-auto text-center">
            <li class="nav-item active">
                <a style="color:#3B3A3C;"class="nav-link" href="index.html">Home  <span class="sr-only"></span></a> 
            </li>
			<li class="nav-item">
                <a style="color:#3B3A3C;" class="nav-link" href="doctors.html">About Us</a>
            </li>
            <li class="nav-item">
                <a style="color:#3B3A3C;" class="nav-link" href="#">Services</a> 
            </li>
            <li class="nav-item">
                <a style="color:#3B3A3C;" class="nav-link" href="testimonials.html">Testimonials</a> 
            </li>
            <li class="nav-item">
                <a style="color:#3B3A3C;" class="nav-link" href="products.html">Products</a> 
            </li>
            <li class="nav-item">
                <a style="color:#3B3A3C;" class="nav-link" href="#">Promos</a> 
            </li> 
            <li class="nav-item">
                <a style="color:#3B3A3C;" class="nav-link" href="blogs.html">Blog</a> 
            </li>
        </ul>
        <ul class="navbar-nav ">
        	    <li class="nav-item text-center">
                <a href="booking.html"><button type="button" class="btn" style="background:#58BFC0;">Book an Appointment </button></a>
            </li>
        </ul>
        <ul class="navbar-nav ml-3 text-center">
			<li>
        	    <i class="fa fa-shopping-cart fa-2x"></i> 
            </li>
        </ul>
         <ul class="navbar-nav ">
        	 <li class="nav-item text-center">
                <a style="color:#3B3A3C;" class="nav-link" href="#">Sign Up</a>  
            </li>
            <li class="nav-item text-center">
                <a style="color:#3B3A3C;" class="nav-link" href="../crud/login.php">Log In</a>  
            </li>
        </ul>
    </div>
</nav>
		<!-- NavBar End-->
		</div>

		<div>
			<img src="img/adminbanner.jpg" class="img-fluid mb-5">
		</div>


		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-10">
					<?php if(isset($_SESSION['response'])){ ?>
					<div class="alert alert-<?= $_SESSION['res_type']; ?> alert-dismissible text-center">
					  <button type="button" class="close" data-dismiss="alert">&times;</button>
					  <b><?= $_SESSION['response']; ?></b>
					</div>
					  <?php } unset($_SESSION['response']); ?>
				</div>
			</div>

			<div class="row justify-content-center">
				<div class="col-3">
					<ul class="list-unstyled mt-2 dashboard">
					<li><i class="fas fa-link"></i>  MY DASHBOARD </li>
					<li><i class="ml-3 fas fa-user-alt py-4"></i>  Doctor's Board</li> 
					<li><a class="patientboard" href="index3.php"> <i class="ml-3 fas fa-user-alt py-4"> </i> Patient's Board </a></li>
					<li><i class="ml-3 fas fa-archive py-4"></i> Manage Services</li>
					<li><i class="ml-3 fas fa-archive py-4"></i> Manage products</li>
					<li><i class="ml-3 fas fa-cogs py-4"></i> Setting</li>
					<li><i class="ml-3 fas fa-sign-out-alt py-4"></i> Log out</li>
					</ul>
				</div>

				<!-- Basic Info -->
				<div class="col-md-9">
					<h3 class="text-center text-info"></h3>
					<form action="action.php" method="post" enctype="multipart/form-data">
						<input type="hidden" name="id" value="<?= $id; ?>">
						<h5> Data Privacy Act</h5>
						<p>This facility is subject to the provisions of Data Privacy Act to ensure the maximum possible<br> level of security. Click here to learn more about our Data Privacy Policy</p>
						<div class="form-group">
							<input type="text" name="name" class="form-control" placeholder="Full name" required value="<?= $name; ?>">
						</div>
						<div class="form-group row">
						  <div class="col-4">

						    <input type="date" class="form-control" id="datefield" name="birthdate" value="Y-m-d" onload="callreadystate">
						  	<script>
					  			var x = '<?php echo date("Y-m-d", strtotime($birthdate));?>'
						  		document.getElementById("datefield").value = x
						  	</script>

						  </div>
						  <div class="col-3">
						    <select class="form-control" name="gender" required>
						    <option value="" selected disabled hidden="">Gender</option>
						    <option value="Male"  <?php if($row['gender']=='Male'){echo "selected";} ?>>Male</option>
						    <option value="Female" <?php if($row['gender']=='Female'){echo "selected";} ?>>Female</option>
						  </select>
						  </div>
						  <div class="col-2">
							<input type="age" name="age" class="form-control" placeholder="Age" required value="<?= $age; ?>">
						  </div>
						  <div class="col-3">
							<input type="mobile" name="mobile" class="form-control" placeholder="Mobile Number" required value="<?= $mobile; ?>" >
						  </div>
						</div>
						<div class="form-group">
							<input type="email" name="email" class="form-control" placeholder="Enter your email" required value="<?= $email; ?>">
						</div>
						<div class="form-group">
							<input type="address" name="address" class="form-control" placeholder="Permanent Address" required value="<?= $address; ?>">
						</div>
						<div class="form-group">
							
						</div>
						<div class="form-group">
							<label for="">How did you hear about us?</label>
							<textarea class="form-control" rows="3" name="aboutus" value=""><?= $aboutus;?></textarea>
						</div>
						<div class="form-group">
							<input type="hidden" name="oldimage" value="<?= $image; ?>">
							<input type="file" name="image" class="custom-file">
							<img src="<?= $image; ?>" width="120" class="img-thumbnail">
						</div>
						<hr>

						<!-- End Basic Info -->

						<!-- Main Reason -->
						 <div class="form-group row"> 
						 	<div class="col-6">
						    <label  class="col-form-label">What is the main reason for your visit today?</label>
						    </div>
						    <div class="col-6">
						    <input type="text" class="form-control" name="reason" value="<?= $reason; ?>"> 
						    </div>
						  </div>
						  <!-- End Main Reason -->

						  <!-- Consultation -->
						   <div class="form-group row"> 
						 	<div class="col-9">
						    <label  class="">Did you have a consultation or treatment for a cosmetic procedure before?</label>
						    </div>
						    <div class="col-3">
									<input type="radio" name="consultation" value="Yes" <?php if($row['consultation']=='Yes'){echo "checked";} ?>> Yes
									<input type="radio" name="consultation" value="No" <?php if($row['consultation']=='No'){echo "checked";} ?>> No
							  </div>
						    </div>
						   <!-- End Consultation -->

						   <!-- Treatment -->
						   <div class="form-group row"> 
						   <div class="col-9">
						    <label  class="">Have you done any procedures or treatment from another clinic before?</label>
						    </div>
						    <div class="col-3">
								<input type="radio" name="treatment" value="Yes" <?php if($row['treatment']=='Yes'){echo "checked";} ?>> Yes
								<input type="radio" name="treatment" value="No" <?php if($row['treatment']=='No'){echo "checked";} ?>> No
							</div>
							<div class="col-3">
						    <label  class="col-form-label">If yes, please specify:</label>
						    </div>
						    <div class="col-9">
						    <input type="text" class="form-control" name="ifyes" value="<?= $ifyes; ?>">
						    </div>
							</div>
							<!-- End Treatment -->

							<!-- Medical History -->
							<H5> Medical History</H5>
								<div class="form-group row">
								<!-- Chronic Illness-->
								<div class="col-6">
							 		<label  class="">1. Do you have any current or chronic illness? </label>
								</div>
								<div class="col-xs">
							 		<input type="radio" name="illness" value="Yes" <?php if($row['illness']=='Yes'){echo "checked";} ?>> Yes
									<input type="radio" name="illness" value="No" <?php if($row['illness']=='No'){echo "checked";} ?>> No
								</div>
								</div>	
								<!-- End Chronic Illness-->

								<!-- Implants -->
								<div class="form-group row">
								<div class="col-6">
							 		<label  class="col-form-label">2. Do you have any implants in your body? </label>
								</div>
								<div class="col-xs">
							 		<select class="form-control" name="implants" required>
									    <option value="" selected disabled hidden="">Implants</option>
									    <option value="Metal" <?php if($row['implants']=='Metal'){echo "selected";} ?>>Metal</option>
									    <option value="Silicone" <?php if($row['implants']=='Silicone'){echo "selected";} ?> >Silicone</option>
									    <option value="Collagen" <?php if($row['implants']=='Collagen'){echo "selected";} ?>>Collagen</option>
									    <option value="Others" <?php if($row['implants']=='Others'){echo "selected";} ?>>Others</option>
									 </select>
								</div>
								</div>
								<!-- End Implants -->

								<!-- Allergies -->
								<div class="form-group row">
								<div class="col-4">
							 		<label  class="">3.Do you have any allergies? </label>
								</div>
								<div class="col-xs">
							 		<input type="radio" name="allergies" value="Yes" <?php if($row['allergies']=='Yes'){echo "checked";} ?>> Yes
									<input type="radio" name="allergies" value="No" <?php if($row['allergies']=='No'){echo "checked";} ?>> No
								</div>
								</div>
								<!-- End Allergies -->

								<!-- Pregnant -->
								<div class="form-group row">
								<div class="col-4">
							 		<label  class="">4.Are you pregnant/lactating? </label>
								</div>
								<div class="col-xs">
							 		<input type="radio" name="pregnant" value="Yes" <?php if($row['pregnant']=='Yes'){echo "checked";} ?>> Yes
									<input type="radio" name="pregnant" value="No" <?php if($row['pregnant']=='No'){echo "checked";} ?>> No
								</div>
								</div>
								<!-- End Pregnant -->

								<!-- Medications -->
								<div class="form-group row">
								<div class="col-7">
							 		<label  class="col-form-label">5. Select medications you've taken for the last 6 months: </label>
								</div>
								<div class="col-3">
							 		<select class="form-control" name="medications" required>
									    <option value="" selected disabled hidden="">Medications</option>
									    <option value="Medication1" <?php if($row['medications']=='Medication1'){echo "selected";} ?>>Medication 1</option>
									    <option value="Medication2" <?php if($row['medications']=='Medication2'){echo "selected";} ?>>Medication 2</option>
									    <option value="Medication3" <?php if($row['medications']=='Medication3'){echo "selected";} ?>>Medication 3</option>
									    <option value="Medication4" <?php if($row['medications']=='Medication4'){echo "selected";} ?>>Medication 4</option>
									 </select>
								</div>
								</div>
								<!-- End Medications -->

								<!-- Exposure -->
								<div class="form-group row">
								<div class="col-10">
							 		<label>6. Have you had any unprotected sun exposure/used tanning creams or tanning beds in the last 4-6 weeks? </label>
								</div>
								<div class="col">
							 		<input type="radio" name="exposure" value="Yes" <?php if($row['exposure']=='Yes'){echo "checked";} ?>> Yes
									<input type="radio" name="exposure" value="No" <?php if($row['exposure']=='No'){echo "checked";} ?>> No
								</div>
								</div>
								<!-- End Exposure -->
								<hr>

								<!-- Treatment Results -->
								<div class="form-group row">
									<div class="col-12">
									<span>I would be very <b>happy</b> if I could achieve the following result(s) after the treatment: </span>
									</div>

									<div class="radio">
									  <div class="col-12">	
									  <input type="checkbox" name="treatmentres[]" value="lesstired" <?php if(in_array("lesstired", $atreat)) {echo "checked";}?>> Less Tired 
									  <input type="checkbox" name="treatmentres[]" value="morerefreshed" <?php if(in_array("morerefreshed", $atreat)) {echo "checked";}?>> More refreshed 
									  <input type="checkbox" name="treatmentres[]" value="lesspigmentation"<?php if(in_array("lesspigmentation", $atreat)) {echo "checked";}?>> Less Pigmentation
									  </div>
									  <div class="col-12">
									  <input type="checkbox" name="treatmentres[]" value="moreconfident" <?php if(in_array("moreconfident", $atreat)) {echo "checked";}?>> More Confident 
									  <input type="checkbox" name="treatmentres[]" value="lesssad" <?php if(in_array("lesssad", $atreat)) {echo "checked";}?>> Less Sad
									  <input type="checkbox" name="treatmentres[]" value="younger" <?php if(in_array("younger", $atreat)) {echo "checked";}?>> Younger
									  </div>
									  <div class="col-12">
									  <input type="checkbox" name="treatmentres[]" value="lesssaggy" <?php if(in_array("lesssaggy", $atreat)) {echo "checked";}?>> Less Saggy
									  <input type="checkbox" name="treatmentres[]" value="moreattractive" <?php if(in_array("moreattractive", $atreat)) {echo "checked";}?>> More Attractive
									  <input type="checkbox" name="treatmentres[]" value="lessangry" <?php if(in_array("lessangry", $atreat)) {echo "checked";}?>> Less Angry/Serious
									  <input type="checkbox" name="treatmentres[]" value="femmacho" <?php if(in_array("femmacho", $atreat)) {echo "checked";}?>> More Feminine/Macho
									</div>
									</div>
								</div>
								<!-- End Treatment Results -->

								<div class="form-group row">
									<div class="col-12">
									<span>When looking at my face in the mirror, I look <b>younger</b>, <b>the same as</b> or <b>older</b> than my true age: </span>
									</div>

									<div class="radio">
									  <div class="col-12">	
									  <input type="radio" name="mirrorage" value="Younger" <?php if($row['mirrorage']=='Younger'){echo "checked";} ?>> Younger than my age
									  <input type="radio" name="mirrorage" value="Same as my Age"  <?php if($row['mirrorage']=='Same as my Age'){echo "checked";} ?>> The same as my true age
									  <input type="radio" name="mirrorage" value="Older than my Age"  <?php if($row['mirrorage']=='Older than my Age'){echo "checked";} ?>> Older than my true age
									  </div>
									 </div>
								</div>
								<!-- End Treatment Results -->

								<div class="form-group row">
									<div class="col-12">
									<label for="">These treatment/products interests me: Please check the treatment(s) of your interest:</label>
									</div>

									<div class="col-4">
										<!-- Skin Enhancement -->
									  <button class="btn btn-light dropdown-toggle mr-4" type="button" data-toggle="dropdown"
									          aria-haspopup="true" aria-expanded="false">SKIN ENHANCEMENT</button>

									  <div class="dropdown-menu">
									    <a class="dropdown-item">
									      <!-- Default unchecked -->
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="facial" name="skinenhance[]" <?php if(in_array("facial", $ase)) {echo "checked";}?>>Facial
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="lasertreatment" name="skinenhance[]" <?php if(in_array("lasertreatment", $ase)) {echo "checked";}?>>Laser Treatment
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="peeling" name="skinenhance[]" <?php if(in_array("peeling", $ase)) {echo "checked";}?>>Peeling
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="glowingskin" name="skinenhance[]" <?php if(in_array("glowingskin", $ase)) {echo "checked";}?>>Glowing Skin
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="textureimprovement" name="skinenhance[]" <?php if(in_array("textureimprovement", $ase)) {echo "checked";}?>>Texture Improvement
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="skintightening" name="skinenhance[]" <?php if(in_array("skintightening", $ase)) {echo "checked";}?>>Skin Tightening
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="porereduction" name="skinenhance[]" <?php if(in_array("porereduction", $ase)) {echo "checked";}?>>Pore Reduction
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="acnescars" name="skinenhance[]" <?php if(in_array("acnescars", $ase)) {echo "checked";}?>>Acne & Acne Scars
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="stretchmarks" name="skinenhance[]" <?php if(in_array("stretchmarks", $ase)) {echo "checked";}?>>Stretch Marks
										  </label>
										  </div>
									    </a>
									  </div>
									  <!-- End Skin Enhancement -->
									</div>
									
									<div class="col-4">
										<!-- Facial Improvement -->
									  <button class="btn btn-light dropdown-toggle mr-4" type="button" data-toggle="dropdown"
									          aria-haspopup="true" aria-expanded="false">FACIAL IMPROVEMENT</button>

									  <div class="dropdown-menu">
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="facialfilters" name="facialimprovement[]" <?php if(in_array("facialfilters", $ase)) {echo "checked";}?>>Facial Filters
										  </label>
										  </div>
									    </a>									    
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="wrinklerelaxers" name="facialimprovement[]" <?php if(in_array("wrinklerelaxers", $afi)) {echo "checked";}?>>Wrinkle Relaxers
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="threadlift" name="facialimprovement[]" <?php if(in_array("threadlift", $afi)) {echo "checked";}?>>Thread lift
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="eyebrowtatt" name="facialimprovement[]" <?php if(in_array("eyebrowtatt", $afi)) {echo "checked";}?>>Eyebrow Tattoo
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
										  <label class="form-check-label">
										    <b>Body Conditioning</b>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="facialfilters" name="facialimprovement[]" <?php if(in_array("facialfilters", $afi)) {echo "checked";}?>>Facial Lifters
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="wrinklerelaxers" name="facialimprovement[]" <?php if(in_array("wrinklerelaxers", $afi)) {echo "checked";}?>>Wrinkle Relaxers
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="threadlift" name="facialimprovement[]" <?php if(in_array("threadlift", $afi)) {echo "checked";}?>>Thread Lift
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="eyebrowtatt" name="facialimprovement[]" <?php if(in_array("eyebrowtatt", $afi)) {echo "checked";}?>>Eyebrow Tattoo
										  </label>
										  </div>
									    </a>
									  </div>
									  <!-- End Facial Improvement -->
									</div>

									<div class="col-4">
										<!-- Others -->
									  <button class="btn btn-light dropdown-toggle mr-4" type="button" data-toggle="dropdown"
									          aria-haspopup="true" aria-expanded="false">OTHERS</button>

									  <div class="dropdown-menu">	
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="laserremoval" name="others[]" <?php if(in_array("laserremoval", $ao)) {echo "checked";}?>>Laser Hair Removal
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="hairgrowth" name="others[]" <?php if(in_array("hairgrowth", $ao)) {echo "checked";}?>>Hair Regrowth
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="tattremoval" name="others[]" <?php if(in_array("tattremoval", $ao)) {echo "checked";}?>>Tattoo Removal
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="birthmarks" name="others[]" <?php if(in_array("birthmarks", $ao)) {echo "checked";}?>>Birthmarks
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="moleremoval" name="others[]" <?php if(in_array("moleremoval", $ao)) {echo "checked";}?>>Mole Removal
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="skinbiopsy" name="others[]" <?php if(in_array("skinbiopsy", $ao)) {echo "checked";}?>>Skin Biopsy
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="stressmanagement" name="others[]" <?php if(in_array("stressmanagement", $ao)) {echo "checked";}?>>Stress Management
										  </label>
										  </div>
									    </a>
									    <a class="dropdown-item" href="#">
									      <div class="form-check">
										  <label class="form-check-label">
										    <input type="checkbox" class="form-check-input" value="overallwellness" name="others[]" <?php if(in_array("overallwellness", $ao)) {echo "checked";}?>>Overall Wellness
										  </label>
										  </div>
									    </a>
									  </div>
									  <!-- End Others -->
									</div>
									<!-- End Treatment Results -->
	
								</div>
							</label>	
						</a>

								<div class="form-group">
							    <div class="form-check">
							      <input class="form-check-input" type="checkbox" id="gridCheck" required>
							      <label class="form-check-label" for="gridCheck" >
							        By submitting this form, you agree to the Terms & Conditions of this website.
							      </label>
							    </div>
							  </div>


						<div class="form-group">
							<?php if($update==true){ ?>
							<input type="submit" name="update" class="btn btn-success btn-block" value="Update Record">
							<?php } else { ?>
							<input type="submit" name="add" class="btn btnsave" value="SAVE AND CLOSE">
							<?php } ?>
						</div>
					</form>
				</div>

				<div>
			</div>
		</div>
</div>
		<!-- Footer -->
		    <footer id="sticky-footer" class="py-2 text-white-50" style="background:#C4E2BF;">
		   		<div class="container text-center">
		      		<small style="color:#3B3A3C;">Copyright &copy; 2019. Everglow All Rights Reserved</small><br>
		      		<small style="color:#3B3A3C;">FAQ's | No Show Policy | Data Privacy Notice</small>
		    	</div>
		  	</footer>
		<!-- End Footer -->

		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script></body>
</html>