 const pics = document.querySelector(".testimonial-pics");
 const textFull = document.querySelector(".testimonial-contents");

for (pic of pics.children) {
  pic.addEventListener("click", function() {
    for (elem of pics.children) {
      elem.classList.remove("active");
    }
    this.classList.add("active");

    const current = Number(this.getAttribute("id"));

    for (text of textFull.children) {
      text.classList.remove("active");
    }
    textFull.children[current].classList.add("active");
  });
}


// Scroll to specific values
// scrollTo is the same
window.scroll({
  top: 2500, 
  left: 0, 
  behavior: 'smooth'
});

// Scroll certain amounts from current position 
window.scrollBy({ 
  top: 100, // could be negative value
  left: 0, 
  behavior: 'smooth' 
});

// Scroll to a certain element
document.querySelector('.hello').scrollIntoView({ 
  behavior: 'smooth' 
<<<<<<< HEAD
});
=======
});


>>>>>>> 514980ff968c7b61718ffd2f710cf50abb40a784
