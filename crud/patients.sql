-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2020 at 08:48 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `everglow`
--

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `address` varchar(50) NOT NULL,
  `gender` varchar(25) NOT NULL,
  `birthdate` varchar(25) NOT NULL,
  `age` int(11) NOT NULL,
  `email` varchar(25) NOT NULL,
  `mobile` int(25) NOT NULL,
  `aboutus` text NOT NULL,
  `image` varchar(225) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `consultation` varchar(3) NOT NULL,
  `treatment` varchar(3) NOT NULL,
  `ifyes` varchar(255) NOT NULL,
  `illness` varchar(255) NOT NULL,
  `implants` varchar(255) NOT NULL,
  `allergies` varchar(255) NOT NULL,
  `exposure` varchar(255) NOT NULL,
  `pregnant` varchar(255) NOT NULL,
  `medications` varchar(255) NOT NULL,
  `treatmentres` varchar(255) NOT NULL,
  `mirrorage` varchar(22) NOT NULL,
  `skinenhance` varchar(255) NOT NULL,
  `facialimprovement` varchar(255) NOT NULL,
  `others` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`id`, `name`, `address`, `gender`, `birthdate`, `age`, `email`, `mobile`, `aboutus`, `image`, `reason`, `consultation`, `treatment`, `ifyes`, `illness`, `implants`, `allergies`, `exposure`, `pregnant`, `medications`, `treatmentres`, `mirrorage`, `skinenhance`, `facialimprovement`, `others`) VALUES
(2, 'Bruce Wayne', 'Gotham', 'Male', 'June 20, 1990', 30, 'bwayne@gmail.com', 928485345, 'From Joker', 'uploads/4.PNG', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(3, 'Clark Kent', 'New York', 'Male', 'June 24, 1990', 30, 'ckent@gmail.com', 2147483647, 'The newspaper ', 'uploads/3.PNG', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4, 'Jane Doe', 'New Zealand', 'Female', 'October 3, 1990', 30, 'jdoe@gmail.com', 928934284, 'Instagram ', 'uploads/1.PNG', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(5, 'Angelica Panganiban', 'Makati', 'Female', 'February 29, 1991', 29, 'angelica.panganiban@gmail', 928435345, 'ABS-CBN ', 'uploads/6.PNG', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(6, 'Nadine Lustre', 'Bonifacio Global City', 'Female', 'June 05, 1994', 26, 'nadine.lustre@gmail.com', 2147483647, 'GMA ', 'uploads/2.PNG', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7, 'Kathryn Bernardo', 'BGC', 'Female', 'August 02, 1996', 23, 'kbernardo@gmail.com', 928347324, 'Facebook ', 'uploads/', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8, 'Test 1', 'Test 1', 'Male', '2020-02-27', 24, 'mit.hau0123@gmail.com', 2147483647, 'Test 1', 'uploads/', 'Test 1', 'Yes', 'Yes', 'Test 1', '', '', '', '', '', '', '', '', '', '', ''),
(9, 'Res 1', '#30 Citio Sentro, Brgy. Sta. Lucia, Lubao', 'Male', '2020-02-27', 23, 'victortamayo08@gmail.com', 2147483647, 'Res 1', 'uploads/', 'Res 1', 'Yes', 'Yes', 'Res 1', '', '', '', '', '', '', 'lesstired,moreconfident,lesssaggy', '', '', '', ''),
(10, 'Test29', 'Test29', 'Female', '2020-02-29', 29, 'Test29@gmail.com', 12412413, 'Test29', 'uploads/', 'Test29', 'Yes', 'Yes', 'Test29', '', '', '', '', '', '', 'lesstired,moreconfident,lesssad,moreattractive', 'younger', '', '', ''),
(11, 'Test12', 'Test12', 'Male', '2020-03-12', 23, 'Test12@gmail.com', 123123213, 'Test12', 'uploads/', 'Test12', 'Yes', 'Yes', 'Test12', '', '', '', '', '', '', 'lesstired,morerefreshed,moreconfident', 'same', 'skintightening,porered', '', ''),
(12, 'Test12', 'Test12', 'Male', '2020-03-12', 23, 'Test12@gmail.com', 123123213, 'Test12', 'uploads/', 'Test12', 'Yes', 'Yes', 'Test12', '', '', '', '', '', '', 'lesstired,morerefreshed,moreconfident', 'same', 'glowingskin,skintightening,porereduction,acnescars,stretchmarks', '', ''),
(13, 'Test12', 'Test12', 'Male', '2020-03-12', 23, 'Test12@gmail.com', 123123213, 'Test12', 'uploads/', 'Test12', 'Yes', 'Yes', 'Test12', '', '', '', '', '', '', 'lesstired,morerefreshed,moreconfident', 'same', 'glowingskin,skintightening,porereduction,acnescars,stretchmarks', 'threadlift,eyebrowtatt,wrinklerelaxers,eyebrowtatt', ''),
(14, 'test45', 'test45', 'Male', '2020-03-01', 23, 'test45@gmail.com', 12323123, 'test45', 'uploads/', 'test45', 'Yes', 'Yes', 'test45', '', '', '', '', '', '', 'younger,femmacho', 'same', 'textureimprovement,skintightening,porereduction,acnescars', 'threadlift,eyebrowtatt,eyebrowtatt', 'birthmarks,moleremoval,skinbiopsy,stressmanagement,overallwellness'),
(15, 'test35', 'test35', 'Male', '2020-03-01', 23, 'test35@gmail.com', 412341234, 'test35', 'uploads/', 'test35', 'Yes', 'Yes', 'test35', '', '', '', '', '', '', 'morerefreshed,lessangry', 'same', 'porereduction,acnescars', 'eyebrowtatt,eyebrowtatt', 'stressmanagement,overallwellness'),
(16, 'test35', 'test35', 'Male', '', 23, 'test35@gmail.com', 412341234, 'test35', 'uploads/', 'test35', 'Yes', 'Yes', 'test35', '', '', '', '', '', '', 'morerefreshed,lessangry', 'same', 'porereduction,acnescars', 'eyebrowtatt,eyebrowtatt', 'stressmanagement,overallwellness'),
(17, 'Sample 1', 'Purok 3 Lakandula 7th Street Lakandula', 'Male', '2020-03-02', 12, 'Sample1@gmail.com', 929393993, 'Facebook', 'uploads/', 'Sample 1', 'Yes', 'No', '', '', 'Metal', '', '', '', '', 'lesstired,moreconfident,lesssaggy', 'younger', 'porereduction,acnescars,stretchmarks', 'eyebrowtatt,threadlift,eyebrowtatt', 'stressmanagement'),
(18, 'Sample 2', 'Sample 1', 'Male', '2020-03-02', 23, 'sample2@gmail.com', 91237237, 'Sample 1', 'uploads/', 'Follow up', 'No', 'Yes', 'Sample 1', '', 'Collagen', 'Yes', '', '', '', 'lesstired,moreconfident,lesssaggy', 'younger', 'stretchmarks', 'eyebrowtatt,eyebrowtatt', 'overallwellness'),
(20, 'Juan dela Cruz', 'asdfasdf', 'Female', '2020-03-03', 23, 'juandelacruz@gmail.com', 98672347, 'Through social media', 'uploads/PatientDisplayForm.jpg', 'Follow upsss', 'Yes', 'Yes', 'Laser', 'Yes', 'Metal', 'No', 'Medication2', 'No', '', 'lesstired,moreconfident,lesssaggy', 'Younger', 'skintightening,porereduction,acnescars,stretchmarks', 'threadlift,eyebrowtatt,threadlift,eyebrowtatt', 'skinbiopsy,stressmanagement,overallwellness'),
(21, 'Pablo Escobar', 'Mexico', 'Male', '2020-03-04', 23, 'pabloescobar@gmail.com', 92876453, 'Social Media', 'uploads/Lock Screen.jpg', 'Follow Up', 'Yes', 'Yes', 'Laser Treatment', 'Yes', 'Metal', 'Yes', 'Medication2', 'Yes', 'Yes', 'lesstired,moreconfident,lesssaggy', 'Younger', 'stretchmarks', 'eyebrowtatt,eyebrowtatt', 'overallwellness'),
(22, 'LeBron James', 'New York', 'Male', '2020-03-04', 23, 'lbj@gmail.com', 929847378, 'Social Media', 'uploads/Wallpaper.png', 'Follow Up', 'Yes', 'Yes', 'Follow Up', 'Yes', 'Metal', 'Yes', 'Medication1', 'Yes', 'Yes', 'lesstired,moreconfident,lesssaggy', 'Younger', 'acnescars', 'wrinklerelaxers', 'overallwellness'),
(23, 'Dwayne Wade', 'New York', 'Male', '2020-03-04', 23, 'dwade@gmail.com', 929847378, 'Social Media', 'uploads/Wallpaper.png', 'Follow Up', 'Yes', 'Yes', 'Follow Up', 'Yes', 'Metal', 'Yes', 'Medication1', 'Yes', 'Yes', 'lesstired,moreconfident,lesssaggy', 'Younger', 'acnescars', 'wrinklerelaxers', 'overallwellness'),
(24, 'Stephen Curry', 'New York', 'Male', '2020-03-04', 23, 'scurry@gmail.com', 929847378, 'Social Media', 'uploads/Wallpaper.png', 'Follow Up', 'Yes', 'Yes', 'Follow Up', 'Yes', 'Metal', 'Yes', 'Medication2', 'Yes', 'Yes', 'lesstired,moreconfident,lesssaggy', 'Younger', 'acnescars', 'wrinklerelaxers', 'overallwellness'),
(25, 'Michael Jordan', 'New York', 'Male', '2020-03-04', 23, 'mjordan@gmail.com', 929847378, 'Social Media', 'uploads/Wallpaper.png', 'Follow Up', 'Yes', 'Yes', 'Follow Up', 'Yes', 'Metal', 'Yes', 'Medication2', 'No', 'Yes', 'lesstired,moreconfident,lesssaggy', 'Younger', 'acnescars', 'wrinklerelaxers,wrinklerelaxers', 'overallwellness'),
(27, 'Last 5', 'Makati', 'Male', '2020-03-04', 23, 'last5@gmail.com', 91234893, 'Social Media', 'uploads/Lock Screen.jpg', 'Follow up', 'Yes', 'Yes', 'Last 5', 'Yes', 'Metal', 'Yes', 'Yes', 'Yes', 'Medication1', 'lesstired,moreconfident,lesssaggy', 'Younger', 'stretchmarks', 'eyebrowtatt,eyebrowtatt', 'overallwellness');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
