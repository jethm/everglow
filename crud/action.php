<?php 
	
	session_start();
	include 'config.php';

	$fields = ['id','consultation','exposure','implants','allergies','pregnant','mirrorage','medications','illness','name','address','gender','birthdate','age','treatmentres','skinenhance','facialimprovement','others','email','medications','mobile','aboutus','reason','ifyes','treatment','image'];
	if(!isset($row)) {
		foreach ($fields as $f) {
			$row[$f] = false;
		}
	}

	$atreat=[];
	$ase = [];
	$afi = [];
	$ao=[];

	$update=false;
	$id='';
	$name='';
	$address='';
	$gender='';
	$birthdate='';
	$age='';
	$email='';
	$mobile='';
	$aboutus='';
	$image='';
	$reason='';
	$ifyes='';
	$consultation='';
	$treatment='';
	$exposure='';
	$medications='';



	if(isset($_POST['add'])) {
		$name=$_POST['name'];
		$address=$_POST['address'];
		$gender=$_POST['gender'];
		$birthdate=$_POST['birthdate'];
		$age=$_POST['age'];
		$email=$_POST['email'];
		$mobile=$_POST['mobile'];
		$aboutus=$_POST['aboutus'];
		$image=$_FILES['image']['name'];
		$upload="uploads/".$image;
		$reason=$_POST['reason'];
		$consultation=$_POST['consultation'];
		$treatment=$_POST['treatment'];
		$ifyes=$_POST['ifyes'];
		$illness=$_POST['illness'];
		$implants=$_POST['implants'];
		$allergies=$_POST['allergies'];
		$pregnant=$_POST['pregnant'];
		$exposure=$_POST['exposure'];
		$medications=$_POST['medications'];
		$treatmentres=$_POST['treatmentres'];
		$treat=implode(',', $treatmentres);	
		$mirrorage=$_POST['mirrorage'];
		$skinenhance=$_POST['skinenhance'];
		$se=implode(',', $skinenhance);
		$facialimprovement=$_POST['facialimprovement'];
		$fi=implode(',', $facialimprovement);
		$others=$_POST['others'];
		$o=implode(',', $others);

		$query="INSERT INTO patients(name,address,gender,birthdate,age,email,mobile,aboutus,image,reason,consultation,treatment,ifyes,illness,implants,allergies,pregnant,medications,exposure,treatmentres,mirrorage,skinenhance,facialimprovement,others)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$statement=$conn->prepare($query);
		$statement->bind_param("ssssssssssssssssssssssss",$name,$address,$gender,$birthdate,$age,$email,$mobile,$aboutus,$upload,$reason,$consultation,$treatment,$ifyes,$illness,$implants,$allergies,$pregnant,$medications,$exposure,$treat,$mirrorage,$se,$fi,$o);
		$statement->execute();
		move_uploaded_file($_FILES['image']['tmp_name'], $upload);

		header('location:index3.php');
		$_SESSION['response']="Successfully inserted into the database!";
		$_SESSION['res_type']="success";

	}

	if(isset($_GET['delete'])){
		$id=$_GET['delete'];

		$sql="SELECT image FROM patients WHERE id=?";
		$statement2=$conn->prepare($sql);
		$statement2->bind_param("i",$id);
		$statement2->execute();
		$result2=$statement2->get_result();
		$row=$result2->fetch_assoc();

		$imagepath=$row['image'];
		unlink($imagepath);


		$query="DELETE FROM patients WHERE id=?";
		$statement=$conn->prepare($query);
		$statement->bind_param("i",$id);
		$statement->execute();

		header('location:index3.php');
		$_SESSION['response']="Successfully deleted!";
		$_SESSION['res_type']="danger";
	}

	if (isset($_GET['index2'])) {
		$id=$_GET['index2'];
		$query="SELECT * FROM patients WHERE id=?";
		$statement=$conn->prepare($query);
		$statement->bind_param("i",$id);
		$statement->execute();
		$result=$statement->get_result();
		$row=$result->fetch_assoc();

		$id=$row['id'];
		$name=$row['name'];
		$address=$row['address'];
		$gender=$row['gender'];
		$birthdate=$row['birthdate'];
		$age=$row['age'];
		//Treatmentres
		$treat=$row['treatmentres'];
		$atreat=explode(',', $treat);
		//End Treatmentres

		//Skinenchance
		$se=$row['skinenhance'];
		$ase=explode(',', $se);
		//End Skinenhance

		//Facial Improvement
		$fi=$row['facialimprovement'];
		$afi=explode(',', $fi);
		//End Facial Improvement

		//Others
		$o=$row['others'];
		$ao=explode(',', $o);
		//End Others

		$email=$row['email'];
		$mobile=$row['mobile'];
		$aboutus=$row['aboutus'];
		$reason=$row['reason'];
		$ifyes=$row['ifyes'];
		$pregnant=$row['pregnant'];
		$medications=$row['medications'];
		$exposure=$row['exposure'];
		$treatment=$row['treatment'];
		$consultation=$row['consultation'];
		$image=$row['image'];
		$update=true;


	}

	if(isset($_POST['update'])){
		$id=$_POST['id'];
		$name=$_POST['name'];
		$address=$_POST['address'];
		$gender=$_POST['gender'];
		$birthdate=$_POST['birthdate'];
		$age=$_POST['age'];
		$email=$_POST['email'];
		//Treatmentres
		$b=$_REQUEST['treatmentres'];
		$c=implode(',', $b);
		//End Treatmentres

		//Skinenhance
		$d=$_REQUEST['skinenhance'];
		$e=implode(',', $d);
		//End Skinenhance

		//Facial Improvement
		$f=$_REQUEST['facialimprovement'];
		$g=implode(',', $f);
		//End Facial Improvement

		//Others
		$h=$_REQUEST['others'];
		$i=implode(',', $h);
		//End Others

		$reason=$_POST['reason'];
		$consultation=$_POST['consultation'];
		$treatment=$_POST['treatment'];
		$ifyes=$_POST['ifyes'];
		$illness=$_POST['illness'];
		$implants=$_POST['implants'];
		$allergies=$_POST['allergies'];
		$pregnant=$_POST['pregnant'];
		$exposure=$_POST['exposure'];
		$medications=$_POST['medications'];
		$mirrorage=$_POST['mirrorage'];
		$mobile=$_POST['mobile'];
		$aboutus=$_POST['aboutus'];
		$oldimage=$_POST['oldimage'];

		if(isset($_FILES['image']['name'])&&($_FILES['image']['name']!="")) {
			$newimage="uploads/".$_FILES['image']['name'];
			unlink($oldimage);
			move_uploaded_file($_FILES['image']['tmp_name'], $newimage);
		}
		else {
			$newimage=$oldimage;
		}
		$query="UPDATE patients SET name=?,address=?,gender=?,birthdate=?,age=?,email=?,mobile=?,aboutus=?,reason=?,consultation=?,treatment=?,ifyes=?,illness=?,implants=?,allergies=?,pregnant=?,exposure=?,medications=?,mirrorage=?,image=?,treatmentres=?,skinenhance=?,facialimprovement=?,others=? WHERE id=?";
		$statement=$conn->prepare($query);
		$statement->bind_param("ssssssssssssssssssssssssi",$name,$address,$gender,$birthdate,$age,$email,$mobile,$aboutus,$reason,$consultation,$treatment,$ifyes,$illness,$implants,$allergies,$pregnant,$exposure,$medications,$mirrorage,$newimage,$c,$e,$g,$i,$id);
		$statement->execute();

		header("location:index3.php");
		$_SESSION['response']="Updated Successfully!";
		$_SESSION['res_type']="primary";
	}


	if(isset($_GET['view'])) {
		$id=$_GET['view'];
		$query="SELECT * FROM patients WHERE id=?";
		$statement=$conn->prepare($query);
		$statement->bind_param("i",$id);
		$statement->execute();
		$result=$statement->get_result();
		$row=$result->fetch_assoc();

		$vid=$row['id'];
		$vname=$row['name'];
		$vaddress=$row['address'];
		$vgender=$row['gender'];
		$vbirthdate=$row['birthdate'];
		$vage=$row['age'];
		$vemail=$row['email'];
		$vmobile=$row['mobile'];
		$vaboutus=$row['aboutus'];
		$vimage=$row['image'];
		$vimplants=$row['implants'];
		$vallergies=$row['allergies'];
		$vpregnant=$row['pregnant'];
		$vreason=$row['reason'];
		$vifyes=$row['ifyes'];
		$vtreatment=$row['treatment'];
		$vconsultation=$row['consultation'];
		$vmedications=$row['medications'];
		$vexposure=$row['exposure'];
		$atreat=$row['treatmentres'];
		$vmirrorage=$row['mirrorage'];
		$vse=$row['skinenhance'];
		$vreason=$row['reason'];
		$vtreatment=$row['treatment'];
	}

?>