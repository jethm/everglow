<?php 
	include 'action.php';
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Everglow</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="stylesheet.css">
	</head>
	<body  style="background-color: #e8f5ec;">
		
		<div class="container mt-3">
			<div class="row">
			<div class="col-sm-4">							
			<div class="card">
			  <div class="card-body">
			    <h5 class="card-title text-center"><?= $vname; ?></h5>
			    <img src="<?= $vimage; ?>" alt="" class="rounded-circle mx-auto d-block " style="width:100px;height: 100px;">
			    <p class="font-weight-bold text-center mt-2"><?= $vgender;?></p>
			    <div class="row">
			    <div class="col-12">
			    <h6>Present Address</h6>
			    <p> <?= $vaddress; ?></p>
			    </div>
			    <div class="col">
			    <h6> Mobile Number</h6>
			    <p> <?= $vmobile; ?> </p>
			    </div>
			     <div class="col">
			    <h6> Age </h6>
			    <p> <?= $vage; ?> </p>
			    </div>	
			    <div class="col-12">
			    <h6> Email Address</h6>
			    <p> <?= $vemail; ?> </p>
			    </div>
			    <div class="col-6">
			    <h6> Contact Number</h6>
			    <p> <?= $vmobile; ?> </p>
			    </div>
				</div>
			  </div>
			 </div>
			 <div class="row mt-3">
			 	<div class="col">
			 	<div class="card">
			  	<div class="card-body">
			    <h6> Main reason for today's visist:</h6>
			    <p> <?= $vreason; ?></p>

			    <h6> Previous Cosmetic procedure:</h6>
			    <p> <?= $vtreatment ;?></p>

			    <h6> If yes:</h6>
			    <p> <?= $vifyes; ?></p>

			    <h6> Procedures from another clinic:</h6>
			    <p> <?= $vconsultation;?></p>
			  	</div>
			 	</div>
			 	</div>
			 	</div>
			 	<div class="row mt-3">
			 		<div class="col">
			 			<div class="card">
			 				<div class="card-body">
			 				<h6> How did you hear about us?</h6>
			    			<p> <?= $vaboutus;?></p>
			 				</div>
			 			</div>
			 		</div>
			 	</div>
			</div>


			<div class="col-sm-7">							
			<div class="card">
			  <div class="card-body">
			    <h6 class="card-title">Medical History Board</h6>
			    <div class="row">
			    <div class="col-3">
			    <h6>Body Implants:</h6>
			    <p> <?= $vimplants; ?></p>
			    </div>
			    <div class="col-5">
			    <h6> Medication taken for the last 6 months:</h6>
			    <p><?= $vmedications; ?></p>
			    </div>
			    <div class="col-4">
			    <h6>Sun Exposure:</h6>
			    <p> <?= $vexposure; ?> </p>
			    </div>
			    <div class="col-3">
			    <h6>Allergies:</h6>
			    <p><?= $vallergies; ?></p>
			    </div>
			    <div class="col-6">
			    <h6>Pregnant/Lactating:</h6>
			    <p><?= $vpregnant; ?></p>
			    </div>
				</div>
			  </div>
			 </div>

			 <!-- Treatment Plan-->

			 <div class="row mt-3">
			 	<div class="col">							
				<div class="card">
			 	 <div class="card-body">
			    <h6 class="card-title">Personal Treatment Plan</h6>
			  	</div>
				</div>
			 	</div>
				</div>
			<!-- End Treatment Plan -->

			<!-- Treatment Plan-->
			<div class="row">
			<div class="col-12">
			<div class="card mt-3">
			  <div class="card-body">
			    <div class="row">
			    <div class="col-4">
			    <h6 class="text-center"> Expectations after the Treatment:</h6>
			    <p class="text-center"> <?= $atreat; ?></p>
			    </div>
			    <div class="col-3">
			    <h6 class="text-center"> The mirror says:</h6>
			    <p class="text-center"><?= $vmirrorage; ?></p>
			    </div>
			    <div class="col-5">
			    <h6 class="text-center">Products/Treatments wants by patient:</h6>
			    <p class="text-center"> <?= $vse; ?> </p>
			    </div>
				</div>
			  </div>
			 </div>
			 </div>
	</div>
			<!-- End Treatment Plan -->
			<a  href="index3.php" class="btn btn-danger my-2">Close </a>


</div>
</body>


		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script></body>
</html>