<?php 
	include 'action.php';
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<script src="https://kit.fontawesome.com/de83bf60e8.js" crossorigin="anonymous"></script>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Everglow</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" href="stylesheet.css">
	</head>
	<body>

		<div class="container-fluid" style="padding-left: 0;padding-right: 0">
<!-- NavBar -->
			<nav id="nav-bar" class="navbar fixed-top navbar-expand-md shadow-sm p-3 mb-5 rounded" style="background:#C4E2BF;">
    <a href="" class="navbar-brand mr-0 text-primary"><img id="logo"src="img/everglow.png" style="width:50%;"></a> 
    <button class="navbar-toggler navbar-light ml-1" type="button" data-toggle="collapse" data-target="#collapsingNavbar2" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"> </span>
    </button>
    <div class="navbar-collapse collapse justify-content-center align-items-center w-100" id="collapsingNavbar2">
        <ul class="navbar-nav mx-auto text-center">
            <li class="nav-item active">
                <a style="color:#3B3A3C;"class="nav-link" href="index.html">Home  <span class="sr-only"></span></a> 
            </li>
			<li class="nav-item">
                <a style="color:#3B3A3C;" class="nav-link" href="doctors.html">About Us</a>
            </li>
            <li class="nav-item">
                <a style="color:#3B3A3C;" class="nav-link" href="#">Services</a> 
            </li>
            <li class="nav-item">
                <a style="color:#3B3A3C;" class="nav-link" href="testimonials.html">Testimonials</a> 
            </li>
            <li class="nav-item">
                <a style="color:#3B3A3C;" class="nav-link" href="products.html">Products</a> 
            </li>
            <li class="nav-item">
                <a style="color:#3B3A3C;" class="nav-link" href="#">Promos</a> 
            </li> 
            <li class="nav-item">
                <a style="color:#3B3A3C;" class="nav-link" href="blogs.html">Blog</a> 
            </li>
        </ul>
        <ul class="navbar-nav ">
        	    <li class="nav-item text-center">
                <a href="booking.html"><button type="button" class="btn" style="background:#58BFC0;">Book an Appointment </button></a>
            </li>
        </ul>
        <ul class="navbar-nav ml-3 text-center">
			<li>
        	    <i class="fa fa-shopping-cart fa-2x"></i> 
            </li>
        </ul>
         <ul class="navbar-nav ">
        	 <li class="nav-item text-center">
                <a style="color:#3B3A3C;" class="nav-link" href="#">Sign Up</a>  
            </li>
            <li class="nav-item text-center">
                <a style="color:#3B3A3C;" class="nav-link" href="login.php">Log In</a>  
            </li>
        </ul>
    </div>
</nav>
		<!-- NavBar End-->
		</div>
		
		<div>
			<img src="img/adminbanner.jpg" class="img-fluid mb-5">
		</div>

		
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-10">
					<?php if(isset($_SESSION['response'])){ ?>
					<div class="alert alert-<?= $_SESSION['res_type']; ?> alert-dismissible text-center">
					  <button type="button" class="close" data-dismiss="alert">&times;</button>
					  <b><?= $_SESSION['response']; ?></b>
					</div>
					  <?php } unset($_SESSION['response']); ?>
			</div>
		</div>

			<div class="row">
				<div class="col-3">
					<ul class="list-unstyled mt-3 dashboard">
					<li><i class="fas fa-link"></i>  MY DASHBOARD </li>
					<li><i class="ml-3 fas fa-user-alt py-4"></i>  Doctor's Board</li> 
					<li><a class="patientboard" href="index3.php"> <i class="ml-3 fas fa-user-alt py-4"> </i> Patient's Board </a></li>
					<li><i class="ml-3 fas fa-archive py-4"></i> Manage Services</li>
					<li><i class="ml-3 fas fa-archive py-4"></i> Manage products</li>
					<li><i class="ml-3 fas fa-cogs py-4"></i> Setting</li>
					<li><i class="ml-3 fas fa-sign-out-alt py-4"></i> Log out</li>
					</ul>
				</div>

				<!-- Basic Info -->
				<div class="col-md-9">
					<a  href="index2.php" class="btn btn-add my-2 float-right"> + ADD NEW PATIENT </a>
					<?php 
						$query="SELECT * FROM patients";
						$statement=$conn->prepare($query);
						$statement->execute();
						$results=$statement->get_result();
					 ?>
					<table class="table table-hover">
					    <thead style="background-color:#C4E2BF;">
					      <tr>
					        <th>ID</th>
					        <th>Name</th>
					        <th>Gender</th>
					        <th>Birth Date</th>
					        <th>Mobile</th>
					        <th>Action</th>
					      </tr>
					    </thead>
					    <tbody>
					    	<?php while ($row=$results->fetch_assoc()) { ?>
					      <tr>
					        <td><?= $row['id'];?></td>
					        <td><a href="view.php?view=<?= $row['id']; ?>"><?= $row['name'] ?></a> </td>
					        <td><?= $row['gender']; ?></td>
					        <td><?= $row['birthdate']; ?></td>
					        <td><?= $row['mobile']; ?> </td>
					        <td>
					        	<a href="action.php?delete=<?= $row['id']; ?>" class="badge badge-danger p-2" onclick="return confirm('Do you want to delete this record?');">Delete</a>
					        	<a href="index2.php?index2=<?= $row['id']; ?>" class="badge badge-success p-2">Edit</a>
					        </td>
					      </tr>
					  <?php } ?>
					    </tbody>
					</table>
				</div>
			</div>
		</div>
	</form>
	</div>



				<div>
			</div>
		</div>

		<!-- Footer -->
		    <footer id="sticky-footer" class="py-2 text-white-50" style="background:#C4E2BF;">
		   		<div class="container text-center">
		      		<small style="color:#3B3A3C;">Copyright &copy; 2019. Everglow All Rights Reserved</small><br>
		      		<small style="color:#3B3A3C;">FAQ's | No Show Policy | Data Privacy Notice</small>
		    	</div>
		  	</footer>
		<!-- End Footer -->

		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script></body>
</html>